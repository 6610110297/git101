def insertionSort(num):
    n = len(num) 
    if n <= 1:
        return 
    for i in range(1, n):  
        key = num[i]
        x = i-1
        while x >= 0 and key < num[x]:  
            num[x+1] = num[x]  
            x -= 1
        num[x+1] = key  
  
numbers = [int(x) for x in input().split()]
insertionSort(numbers)
print(numbers)