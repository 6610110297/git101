def selection_sort(number, size):
    for i in range(size):
        min = i
        for x in range(i+1, size):
            if number[x] < number[min] :
                min = x
        (number[i], number[min]) = (number[min], number[i])

    return x
num = [int(x) for x in input().split()]
size = len(num)
selection_sort(num, size)
print(num)